# SDK7 NFTShape Issues Demo

This example scene shows some problems with NFTs in SDK7

1) The first row shows NFTs that have issues with their frames.
    For more information see issue:
    https://github.com/decentraland/sdk/issues/965

2) Both rows, including some NFTs that I've made in the second row, 
   exemplify the failure of NftDialog to fill in its image and text much of the time.
   For more information, see issue:
   https://github.com/decentraland/sdk/issues/971


