import {
  Animator,
  AudioSource,
  AvatarAttach,
  engine,
  GltfContainer,
  InputAction,
  Material,
  MeshCollider,
  pointerEventsSystem,
  Transform,
  VisibilityComponent
} from '@dcl/sdk/ecs'
import { Vector3, Color4 } from '@dcl/sdk/math'
import { NftController } from './modules/NftController'
import { nfts } from './modules/NftInfo'

export function main() {
    
  let scene = engine.addEntity()
  // Give the entity a position via a transform component
  Transform.create(scene, { // todo is this unneeded, does it already have base Transform?
    position: Vector3.create(0, 0, 0)
  })

  let nftController = new NftController(scene, nfts)
}
