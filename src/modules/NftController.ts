////////////////////////////////////////////////////////
// NftController
// MVC Controller class for the gallery NFTs
// (c) 2020, 2023 Carl Fravel
////////////////////////////////////////////////////////

import {
  Vector3, 
  Color3,
  Color4,
  Quaternion
} from '@dcl/sdk/math'

import {NftDisplay, NftProps} from "./NftDisplay"

import { 
  engine,Entity,Transform,TransformTypeWithOptionals,
  Material,PBMaterial,Texture,TextureUnion, PBMaterial_PbrMaterial,
  TextShape, Font, TextAlignMode,
  MeshCollider, MeshRenderer, GltfContainer,
  pointerEventsSystem,InputAction, inputSystem,
  VideoPlayer, PBVideoPlayer,
  executeTask, EventSystemOptions, 
  NftShape, NftFrameType, PBNftShape
} from '@dcl/sdk/ecs'



export type NftInfo =
{
  title:string
  artist:string
  platform:string
  props:Partial<NftProps>
  coords:number[]
}

export class NftController {
  nftDisplays:NftDisplay[]=[]

  constructor(public parent:Entity, nfts:NftInfo[]) {
    for (let nft of nfts) {
      let c = nft.coords
      this.nftDisplays.push(new NftDisplay(
        parent, 
        nft.props,
        c[0],c[1],c[2],  c[3],c[4],c[5],   c[6],c[7],c[8])
      )    
    }
  }
}


