////////////////////////////////
// NftDisplay - CF module for NFT display
// SDK7 version
// (c) 2023 by Carl Fravel
/////////////////////////////

import {
  Vector3, 
  Color3,
  Color4,
  Quaternion
} from '@dcl/sdk/math'

import { 
  engine,Entity,Transform,TransformTypeWithOptionals,
  Material,PBMaterial,Texture,TextureUnion, PBMaterial_PbrMaterial,
  TextShape, Font, TextAlignMode,
  MeshCollider, MeshRenderer, GltfContainer,
  pointerEventsSystem,InputAction, inputSystem,
  VideoPlayer, PBVideoPlayer,
  executeTask, EventSystemOptions, 
  NftShape, NftFrameType, PBNftShape
} from '@dcl/sdk/ecs'

import { openNftDialog } from '~system/RestrictedActions'

export {NftFrameType as NftFrameType} from '@dcl/sdk/ecs'

// this conversion is to allow old database or code props collections to work with strings or numbers, as well as with the new 
let frameNames:string[]=[]
frameNames.push("Classic") // 0,
frameNames.push("Baroque_Ornament") // 1,
frameNames.push("Diamond_Ornament") // 2,
frameNames.push("Minimal_Wide") // 3,
frameNames.push("Minimal_Grey") // 4,
frameNames.push("Blocky") // 5,
frameNames.push("Gold_Edges") // 6,
frameNames.push("Gold_Carved") // 7,
frameNames.push("Gold_Wide") // 8,
frameNames.push("Gold_Rounded") // 9,
frameNames.push("Metal_Medium") // 10,
frameNames.push("Metal_Wide") // 11,
frameNames.push("Metal_Slim") // 12,
frameNames.push("Metal_Rounded") // 13,
frameNames.push("Pins") // 14,
frameNames.push("Minimal_Black") // 15,
frameNames.push("Minimal_White") // 16,
frameNames.push("Tape") // 17,
frameNames.push("Wood_Slim") // 18,
frameNames.push("Wood_Wide") // 19,
frameNames.push("Wood_Twigs") // 20,
frameNames.push("Canvas") // 21,
frameNames.push("None") // 22,


export class NftProps {
  constructor(
    public contract: string,
    public id: string,
    public blockchain:string = "ethereum",
    public contractStandard:string = "erc721",
    
    public style: number | string | NftFrameType = NftFrameType.NFT_MINIMAL_GREY,
    public color: string = "#604000", // medium-dark brown, slightly reddish, which affects NFT_MINIMAL_GREY, making it dark brown/woodish
    public ui: boolean = true,
    public uiText: string = "",
    public maxDistance: number = 2 
  ){}
}

export class NftDisplay{
  rootEntity:Entity
  touchBox:Entity

  constructor (
      public parent: Entity, 
      public props: Partial<NftProps>,
      x:number,y:number,z:number, rx:number, ry:number, rz:number, sx:number, sy:number, sz:number)
  {
    this.rootEntity = engine.addEntity()
    Transform.create(
      this.rootEntity,
      {
        parent:parent,
        position: Vector3.create(x,y,z), // maybe add 0.25 to y?
        rotation: Quaternion.fromEulerDegrees(rx,ry,rz),
        scale: Vector3.create(sx,sy,sz)
      }
    )
    this.touchBox = engine.addEntity()
    Transform.create(this.touchBox,{
      parent:this.rootEntity,
      position:Vector3.create(0,0,0),
      scale:Vector3.create(.25,.25,.05)

    })
    MeshRenderer.setBox(this.touchBox)
    MeshCollider.setBox(this.touchBox)
    Material.setPbrMaterial(this.touchBox,{
      albedoColor:Color4.create(1,1,1,0)
    })

    // let urn:string = "ethereum://0x06012c8cf97bead5deae237070f9587f8e7a266d/558536"
    let urn:string = "urn:decentraland:"+props.blockchain+":"+props.contractStandard+":"+props.contract+":"+props.id
    // cryptokitties urn is, e.g.:
    // let urn:string = 'urn:decentraland:ethereum:erc721:0x06012c8cf97bead5deae237070f9587f8e7a266d:558536'
    let style = (typeof props.style == "string")?frameNames.indexOf(props.style):props.style
    let color = Color3.fromHexString(props.color?props.color:"#FFFFFF")

    console.log("Showing NFT for   "+urn)
    NftShape.create(
      this.rootEntity,
      {
        urn: urn,
        color: color,
        style: style
      }
    )
    if (props.ui) { 
      pointerEventsSystem.onPointerDown(
        {
            entity: this.touchBox,
            opts: {
                button: InputAction.IA_POINTER,
                hoverText: props.uiText,
                showFeedback:true,
                maxDistance:8
            }
        },
        () => {
          console.log("OpenNftDialog for "+urn)
          openNftDialog({
            urn:urn
          })
        }
      )
    }
  }
}


