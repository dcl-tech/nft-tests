////////////////////////////////////
// Example of nft info
// This instance is for the sdk7 nft-tests scene
//////////////////////////////////////////////////////////

import { NftInfo } from './NftController'
import { NftFrameType } from '@dcl/sdk/ecs'

// nfts for nft-tests scene
export const nfts:NftInfo[] =
[
    {
        title:"CryptoKitty",
        artist:"CryptoKitties",
        platform:"OpenSea",
        props: { 
          "blockchain":"ethereum",
          "contractStandard":"erc721",
          "id":"558536", 
          "contract":"0x06012c8cf97bead5deae237070f9587f8e7a266d",
          "style":NftFrameType.NFT_CLASSIC,
          "color":"#604000",
          "ui":true, 
          "uiText":"Click for info",
          "maxDistance":2
        },
        coords:[2.772,1.7,8,  0,0,0,  4,4,4]
    },

    {
        title:"CryptoKitty",
        artist:"CryptoKitties",
        platform:"OpenSea",
        props: {
        "blockchain":"ethereum",
        "contractStandard":"erc721",
        "id":"558536",
        "contract":"0x06012c8cf97bead5deae237070f9587f8e7a266d",
        "style":NftFrameType.NFT_MINIMAL_BLACK,
        "color":"#604000",
        "ui":true,
        "uiText":"Click for info",
        "maxDistance":2
        },
        coords:[5.572,1.7,8,  0,0,0,  4,4,4]
    },


    {
        title:"CryptoKitty",
        artist:"CryptoKitties",
        platform:"OpenSea",
        props: {
        "blockchain":"ethereum",
        "contractStandard":"erc721",
        "id":"558536",
        "contract":"0x06012c8cf97bead5deae237070f9587f8e7a266d",
        "style":NftFrameType.NFT_TAPE,
        "color":"#604000",
        "ui":true,
        "uiText":"Click for info",
        "maxDistance":2
        },
        coords:[8.335,1.7,8,  0,0,0,  4,4,4]
    },

    {
        title:"CryptoKitty",
        artist:"CryptoKitties",
        platform:"OpenSea",
        props: {
        "blockchain":"ethereum",
        "contractStandard":"erc721",
        "id":"558536", 
        "contract":"0x06012c8cf97bead5deae237070f9587f8e7a266d",
        "style":NftFrameType.NFT_WOOD_SLIM,
        "color":"#604000",
        "ui":true, 
        "uiText":"Click for info",
        "maxDistance":2
        },
        coords:[11.079,1.7,8,  0,0,0,  4,4,4]
    }, 

    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////

   
    {
        title:"Blue", // Wondershare.  GIF thumbnail is 256x256 12 fps
        artist:"Carl Fravel",
        platform:"OpenSea",
        props: {
          "blockchain":"ethereum",
          "contractStandard":"erc721",
          "id":"45212788081105388007521116546269099584358446490568136574020829704406356197386", 
          "contract":"0x495f947276749Ce646f68AC8c248420045cb7b5e",
          "style":"Minimal_Grey",
          "color":"#603000",
          "ui":true, 
          "uiText":"Click for info",
          "maxDistance":2
        },
        coords:[1.5,1.7,15,  0,0,0,  4,4,4]
      }, 
    
    
      {
        title:"White",
        artist:"Carl Fravel",
        platform:"OpenSea",
        props: {
          "blockchain":"ethereum",
          "contractStandard":"erc721",
          "id":"45212788081105388007521116546269099584358446490568136574020829709903914336266", 
          "contract":"0x495f947276749Ce646f68AC8c248420045cb7b5e",
          "style":"Minimal_Grey",
          "color":"#603000",
          "ui":true, 
          "uiText":"Click for info",
          "maxDistance":2
        },
        coords:[4,1.7,15,  0,0,0,  4,4,4]
      }, 
    
      {
        title:"Red",
        artist:"Carl Fravel",
        platform:"OpenSea",
        props: {
          "blockchain":"ethereum",
          "contractStandard":"erc721",
          "id":"45212788081105388007521116546269099584358446490568136574020829712102937591818",
          "contract":"0x495f947276749Ce646f68AC8c248420045cb7b5e",
          "style":"Minimal_Grey",
          "color":"#603000",
          "ui":true,
          "uiText":"Click for info",
          "maxDistance":2
        },
        coords:[6.5,1.7,15,  0,0,0,  4,4,4]
      },
    
      // 
    
      {
        title:"Banner",
        artist:"Carl Fravel",
        platform:"OpenSea",
        props: {
          "blockchain":"ethereum",
          "contractStandard":"erc721",
          "id":"45212788081105388007521116546269099584358446490568136574020829713202449219594",
          "contract":"0x495f947276749Ce646f68AC8c248420045cb7b5e",
          "style":"Minimal_Grey",
          "color":"#603000",
          "ui":true,
          "uiText":"Click for info",
          "maxDistance":2
        },
        coords:[9,1.7,15,  0,0,0,  4,4,4]
      },
    
      {
        title:"Before Us There Was Forest",
        artist:"Carl Fravel",
        platform:"OpenSea",
        props: { // V7 256x158 14MB gif thumbnail made in Wondershare.
          "blockchain":"ethereum",
          "contractStandard":"erc721",
          "id":"45212788081105388007521116546269099584358446490568136574020829708804402708490", 
          "contract":"0x495f947276749Ce646f68AC8c248420045cb7b5e",
          "style":"Minimal_Grey",
          "color":"#603000",
          "ui":true,
          "uiText":"Click for info",
          "maxDistance":2
        },
        coords:[11.5,1.7,15,  0,0,0,  4,4,4]
      }

]
